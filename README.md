# How to Protect Your Business From Cyber Crime #

Well as the old saying goes: there is nothing more expensive than cheap labour.

Someone posing as me purporting to be a writer/editor from NYC took a job writing a book for a client and the results were bad. 3 chapters completely plagiarized bad.

Even worse - the fraudulent work got blamed on me! This person's profile had my name and my photo and her client found me (the real me) through a Google search that matched my profile photo. You can imagine the shock I felt of being falsely accused of ripping someone off!

After realizing this was a clear case of identity theft, I immediately contacted UpWork to have the fake profile taken down. This person's client did the same; we hope they were able to get a full refund of the monies spent on the useless book written for them.

Scary stuff. But compared to what others have encountered it's small potatoes.

While hiring and supporting local talent has always been my thing, if you have to outsource (overseas or via those freelance sites), then a great idea to protect yourself is to insist on seeing a scan of government issued ID. Take your identification proof one step further and insist on a video conference before making any commitment.

Fraud is on the rise

2016 saw a significant increase in fraud over 2015. While the numbers show the amount stolen went slightly down the volume of theft went up. A lot.

While those figures relate more to consumer fraud if you're the seller, you can be out of pocket money if the claim means sending the now used product back to you.

The point to take home is fraud is up so you need to take action to prevent yourself and your customers from becoming victims.

How to prevent and report cyber crime

According to The National Cyber Security Alliance there are several steps you should take to protect your business and customers:

• Evaluate Risks

Identify what types of fraud or crime you may be most susceptible to. Do you work with medical information? Financial information?

Even if the purpose of your business is simply B2C there are steps to be taken to protect yourself.

Users who purchase through your website are trusting you to keep their financial information safe so take steps to do so such as having SSL installed for any e-commerce or sensitive information and it's wise not to store it.

• Monitor Threats

This can be as simple as making sure no spam messages are opened or any emails with attachments are scanned with some sort of antivirus software. While the software is not 100% effective it will stop the better circulated scams.

• Report Attacks

If you are the victim of a cyber attack you are going to get frustrated and with good cause.

Currently Canada is really vulnerable when it comes to cyber crime and your best hope is just to call the police. While promises have been made to address this, very little has been done and international criminals are impossible to go after.

If you are a victim of cyber crime contact local law enforcement and cross your fingers. But the bad news is you are likely to get no resolution. This is something to consider if you've been hiring anyone overseas.

In the US reporting cyber crime is much easier. You contact the FBI via this website. They have the capacity to address international criminals and recently America has cracked down on international crime operating within its borders.

For those reading from any other country I encourage you to do your own due diligence regarding protocol for reporting cyber crime so you're prepared should you ever need to be.

• Execute a Security Plan

For this the recommendation is to work with your ISP on a cyber security plan. While your ISP may be worth talking to you should really speak with your website's hosting company first and foremost.

The security of your customer's info and your business is delicate so make sure your host knows to have things such as routine backups of all information made and stored on another server.

Most of the majors stay on top of things but it's always worth calling them for a quick review especially if you have pertinent info for them that may help.

If you have been a victim already let your host know what happened. The information may help others down the road.

• Safeguard Your Clients

The suggestion found in this article of scanning all USB drives routinely is a good one. Sometimes the information can be air tight behind the most advanced firewall but it still gets out.

One of the easiest ways to exploit technology is social engineering. Many times the information isn't so much stolen as leaked by someone internally.

Have a privacy policy in place and make sure your employees know that any time they connect anything to your computer network it will be scanned.

Make sure all software is updated and that all computers connected to your network are running the most updated version of their operating system.

• Educate Your Team

This is an easy one.

Have protocol in place that ensures your employees follow all steps noted above.

All computers must be scanned when attached to a network and all USB drives as well.

Most people are accustomed to this now so don't worry about implementing it suddenly.

Stay Safe

By taking measures to protect yourself you're ahead of the game should something occur. Scrambling after you've been a victim only helps the people who have stolen from you by giving them time to disappear.

Online business is only likely to grow even more and along with it fraud. The complexity of the scams will evolve and hopefully so do the solutions. In the interim I hope you enjoyed these tips and that you never become the victim of cyber crime.

Susan Friesen is the founder of eVision Media, a boutique web development and Digital Marketing firm of over 15 years that specializes in designing, building and marketing professional, unique websites for entrepreneurs, businesses and organizations.

Visit * [Onsist](https://www.onsist.com/executive-protection/)

